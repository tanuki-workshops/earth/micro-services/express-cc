//   A very simple express-js demo, by @k33g
const express = require('express')

const app = express()
const port = process.env.PORT || 8080

app.use(express.static('public'))
app.use(express.json())

app.listen(port, () => console.log(`🌍 webapp is listening on port ${port}!`))
